require("dotenv").config({
  path: `.env`,
})

module.exports = {
  siteMetadata: {
    title: 'Prototype Blog Gallery',
    description: `Optimized webapp with latest cutting edge technologies`,
    author: `Deni Kotsev`,
  },
  plugins: [
    {
      resolve: `gatsby-plugin-create-client-paths`,
      options: { prefixes: [`/user/*`, `/buy/*`] },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-51178779-1",
      }
    },
    {
      resolve: 'gatsby-plugin-material-ui',
      // If you want to use styled components you should change the injection order.
      options: {
        // stylesProvider: {
        //   injectFirst: true,
        // },
      },
    },
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `videos`,
        path: `${__dirname}/videos`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: 'md-pages',
        path: `${__dirname}/src/md`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: ['gatsby-remark-autolink-headers']
      }
    },
    {
      resolve: "gatsby-source-graphql",
      options: {
        // This type will contain remote schema Query type
        typeName: "Blog",
        // This is field under which it's accessible
        fieldName: "blog",
        // Url to query from
        url: process.env.GRAPHCMS_API,
        headers: {
          Authorization: `Bearer ${process.env.GRAPHCMS_PROTO_TOKEN}`,
        },
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Prototype gallery',
        short_name: 'Prototype',
        start_url: '/',
        background_color: '#16708a',
        theme_color: '#143131',
        display: 'minimal-ui',
        icons: [
          {
            src: `/android-chrome-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `/android-chrome-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
        ],
      },
    },
    // 'gatsby-plugin-offline',
    // If you want to use styled components you should add the plugin here.
    // 'gatsby-plugin-styled-components',
    //
    'gatsby-plugin-react-helmet',
  ]
};
