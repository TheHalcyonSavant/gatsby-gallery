import { navigate } from 'gatsby';

export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export async function readyNavigate() {
  while (typeof window.___navigate !== 'function') await sleep(10);
}

export function changeUrl(state) {
  const { pathname, search, hash } = window.location;
  navigate(pathname + search + hash, { state, replace: true });
}

export function log(...args) {
  if (process.env.GATSBY_DEBUG === 'false' || process.env.NODE_ENV === 'production') return;
  console.log(...args);
}
