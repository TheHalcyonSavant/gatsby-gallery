import React from 'react';
import { Router } from '@reach/router';
import Profile from '../user/Profile';
import Jobs from '../user/jobs';
import UserPage from '../user/UserPage';

export default () => (
  <Router>
    <Profile path="/user/profile" />
    <Jobs path="/user/jobs" />
    <UserPage path="/user/:slug" />
  </Router>
);
