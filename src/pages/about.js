import React from 'react';
import { graphql } from 'gatsby'
import Img from 'gatsby-image'
import {
  Card,
  CardContent,
  CardHeader,
  Container,
  Grid,
  Link as MuiLink,
  Typography
} from '@material-ui/core';
import MailIcon from '@material-ui/icons/ContactMail';
import PhoneIcon from '@material-ui/icons/ContactPhone';
import withStyles from '@material-ui/core/styles/withStyles';
import VideoPlayer from '../components/VideoPlayer';
import styles from '../pages.css/about.css';
import burstGreen from './burst-green.png';

export const query = graphql`
  query {
    file(
    	relativeDirectory: { eq: "about" },
      name: { eq: "profile" }
   ) {
      childImageSharp {
        fixed(
          width: 200,
          quality: 100
        ) {
          ...GatsbyImageSharpFixed_tracedSVG
        }
      }
    }
  }
`;

export default withStyles(styles)(({ classes, data }) => {
  return (
    <Container className={classes.root}>
      <Card raised className={classes.card}>
        <CardHeader className={classes.cardHeader} title="Deni Kotsev" />
        <CardContent className={classes.cardContent}>
          <Img fixed={data.file.childImageSharp.fixed} alt="profile" />
          <Grid container className={classes.contactDetails}>
            <Grid item xs={3}><MailIcon fontSize="large" /></Grid>
            <Grid item xs={9}>thehalcyonsavant@gmail.com</Grid>
            <Grid item xs={3}><PhoneIcon fontSize="large" /></Grid>
            <Grid item xs={9}>+49 175 441 6705</Grid>
          </Grid>
        </CardContent>
      </Card>
      <Typography className={classes.header}>Few Notes About This Web App</Typography>
      <Typography className={classes.paragraph}>
        This is a serverless SaaS prototype, maybe exactly the thing you need as I do - a strong template from where you can derive the most optimal products for the future.
        It looks like a simple gallery JAMStack prototype, but there are some interesting hidden features for authorized members.
        Here is the redacted version of them: <MuiLink href="https://gitlab.com/TheHalcyonSavant/redacted-jamstack-gallery">sample codes on GitHub</MuiLink>
      </Typography>
      <Typography className={classes.header}>Why Is It So Awesome?</Typography>
      <Typography className={classes.paragraph}>
        The SSO is created entirely with React and Firebase, you can try to log-in as a guest or with your gmail.
        The menu is statically generated from GraphCMS - a hybrid of structured and unstructured data.
        The entire front-end is built with MaterialUI and Gatsby.
        (gatsby-plugin-manifest makes it PWA compliant, so you don't have to worry about hiring additional iOS or Android developers unnecessarily).
        You'll notice nice loading, fast log-out, fast changes between pages, perfectly coded in Redux-saga.
        Although I've utilized ultramodern techniques for optimization, like having background videos of 100kb, lazy loading of responsive images,
        there is still room for further tightening, like shrinking the MaterialUI production files to max. Any new challenging idea is welcome.
        You can <MuiLink href="mailto:thehalcyonsavant@gmail.com">contact me here</MuiLink>
      </Typography>

      <MuiLink className={classes.header} style={{ textDecoration: 'underline' }} href="https://dict-mng.web.app/">
        Dictionary Manager Webapp <span style={{ background: `url(${burstGreen})` }} className={classes.newBadge}>NEW !</span>
      </MuiLink>
      <Typography className={classes.paragraph}>
        Front-end only webapp for editing and validating dictionary consistency, to mark the duplicate values of table rows with the right error labels.
      </Typography>
      <VideoPlayer url="https://youtu.be/4qu9nxxrIGQ" />
      <Typography className={classes.paragraph}>
        This and the SJS webapp are my most recent projects created with cutting edge skills same as the parent SaaS webapp: <b>React, Redux(-Saga), Gatsby, MaterialUI</b> and <b>GCP</b>.
      </Typography>

      <Typography className={classes.header}>
        Swiss Job Seekers Webapp
      </Typography>
      <Typography className={classes.paragraph}>
        Great solution for Swiss job seekers. Find the best job that fits your skills much easier than before.
      </Typography>
      <VideoPlayer url="https://youtu.be/Yyx3uBHjp14" />
      <Typography className={classes.paragraph}>
        To try the demo, contact me and I'll give you permission, because the demo is still in early release and is only available for few accounts.
      </Typography>

      <Typography className={classes.paragraph}>
        Also, try this attractive animations in the "Buy" page.
      </Typography>
      <VideoPlayer url="https://youtu.be/v8dqHEGg258" playing loop />
      <Typography className={classes.paragraph}>
        <i>The products are test only and the credit cards are not saved, you can explore them safely with any test numbers (like 4** or 5***).</i>
      </Typography>

    </Container>
  );
});
