import React, { useState } from 'react';
import {
  Button,
  Container,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CircularProgress,
  Grid,
  // Link,
  Slide,
  Typography,
} from '@material-ui/core';
import StarBorder from '@material-ui/icons/StarBorder';
import withStyles from '@material-ui/core/styles/withStyles';
import { Match } from '@reach/router';
import CheckAnim from '../components/CheckAnim';
import StripeForm from '../components/StripeForm';
import styles from '../pages.css/buy.css';
import { log, sleep } from '../utils';
import { navigate } from 'gatsby';

const data = [
  {
    id: '1',
    title: 'Product 1',
    price: '500',
    description: Array(4).fill().map((v, i) => `Feature P1-${i}`),
    buttonText: 'Buy online',
    buttonVariant: 'outlined',
  },
  {
    id: '2',
    title: 'Product 2',
    subheader: 'important note',
    price: '2000',
    description: Array(4).fill().map((v, i) => `Feature P2-${i}`),
    buttonText: 'Buy online',
    buttonVariant: 'contained',
  },
  {
    id: '3',
    title: 'Product 3',
    price: '5000',
    description: Array(4).fill().map((v, i) => `Feature P3-${i}`),
    buttonText: 'Contact us',
    buttonVariant: 'outlined',
  },
];
// const footers = [
//   {
//     title: 'Company',
//     description: ['Team', 'History', 'Contact us', 'Locations'],
//   },
//   {
//     title: 'Features',
//     description: ['Cool stuff', 'Random feature', 'Team feature', 'Developer stuff', 'Another one'],
//   },
//   {
//     title: 'Resources',
//     description: ['Resource', 'Resource name', 'Another resource', 'Final resource'],
//   },
//   {
//     title: 'Legal',
//     description: ['Privacy policy', 'Terms of use'],
//   },
// ];

function Price({ value }) {
  return (<>
    <Typography component="h2" variant="h2">${value}</Typography>
    <Typography component="h2" variant="h4">/mo</Typography>
  </>);
}

const Buy = withStyles(styles)(({ classes, productId }) => {
  const [showTier, setShownTier] = useState(null);
  const [checkoutStatus, setCheckoutStatus] = useState(null);
  log('render', productId, showTier);
  const cardStyle = {
    marginTop: '64px',
    width: '322px' // smallest width for stripe cards to fit
  };
  // Similar example to: https://dribbble.com/shots/2293135-Simple-and-intuitive-checkout-form
  async function visitProduct(index) {
    const header = document.querySelector('.rootcontainer header');
    const c = document.querySelectorAll(`.${classes.main} .MuiCard-root`)[index];
    const cActions = c.querySelector('.MuiCardActions-root');
    setCheckoutStatus('pick');
    await sleep(400);
    // TODO: could be refactored with checkoutStatus
    if (cardStyle.width) c.style.width = `${c.offsetWidth}px`;
    // header.style.height = `${header.offsetHeight}px`;
    cActions.style.height = `${cActions.offsetHeight}px`;
    c.style.marginTop = cardStyle.marginTop;
    setShownTier(index); log('setShownTier', index);
    const { left: cLeft, top: cTop } = c.getBoundingClientRect();
    const { top: headerTop } = header.getBoundingClientRect();
    const newX = (document.body.offsetWidth / 2) - cLeft - (parseInt(cardStyle.width || c.offsetWidth) / 2);
    const newY = headerTop - cTop;
    c.style.transform = `translate(${newX + 8}px, ${newY + 12}px)`; // 8 and 12 should be calculated somehow
    window.scrollTo(0, 0);
    await sleep(400);
    if (cardStyle.width) c.style.width = cardStyle.width;
    await sleep(200);
    header.style.opacity = 0;
    // header.style.padding = 0;
    // header.style.height = 0;
    cActions.style.height = 0;
    cActions.style.padding = 0;
    // c.closest('.MuiGrid-container').style.overflow = 'hidden';
    await sleep(2000);
    // Interesting fact: pushState !== ReachRouter's navigate. pushState doesn't render the page/component
    // window.history.pushState(null, '', `?product=${data[index].id}`);
    navigate(`/buy/${data[index].id}`); log('navigate');
    await sleep(800); // needed for animations to stop gracefully
    setCheckoutStatus(null);
    setShownTier(null); log('setShownTier', null);
  }
  // function printTL() {
  //   const newc = document.querySelector(`.${classes.main} .MuiCard-root`);
  //   const { left: newcLeft, top: newcTop } = newc.getBoundingClientRect();
  //   return { newcLeft, newcTop };
  // }
  async function buyProduct() {
    const c = document.querySelector(`.${classes.main} .MuiCard-root`);
    const price = c.querySelector(`.${classes.detachedPrice}`);
    setCheckoutStatus('wait');
    price.style.top = `${Math.round(c.offsetHeight / 2) - 50}px`;
    price.style.left = `${Math.round(c.offsetWidth / 2) - 50}px`;
    await sleep(2000);
    setCheckoutStatus('finished');
  }
  function getCard(tier, index) {
    return (
      <Card style={productId ? cardStyle : {}} className={productId || (checkoutStatus === 'pick') ? 'picked' : ''}>
        <CardHeader
          title={tier.title}
          subheader={tier.subheader}
          titleTypographyProps={{ align: 'center' }}
          subheaderTypographyProps={{ align: 'center' }}
          action={tier.title === 'Pro' ? <StarBorder /> : null}
          className={classes.cardHeader}
        />
        <CardContent
          className={classes.cardContent}>
          {checkoutStatus && checkoutStatus !== 'pick' && <div className={classes.waitOverlay}></div>}
          <div className={classes.price}>
            <div className={classes.staticPrice}>
              {!productId && (checkoutStatus !== 'pick') && <Price value={tier.price} />}
              {productId && <Typography component="h2" variant="h4">Checkout</Typography>}
            </div>
            <div className={classes.detachedPrice}>
              {(!checkoutStatus || checkoutStatus === 'pick') && <Price value={tier.price} />}
              {checkoutStatus === 'wait' && <CircularProgress
                size={50}
                thickness={5} />}
              {checkoutStatus === 'finished' && <CheckAnim />}
            </div>
          </div>
          <ul>
            {tier.description.map(line => (
              <Typography component="li" variant="subtitle1" align="center" key={line}>
                {line}
              </Typography>
            ))}
          </ul>
          {productId && <StripeForm className={classes.form} onPay={buyProduct} />}
        </CardContent>
        {!productId && <CardActions>
          <Button fullWidth variant={tier.buttonVariant} color="secondary" onClick={() => visitProduct(index)}>
            {tier.buttonText}
          </Button>
        </CardActions>}
      </Card>
    );
  }
  
  return (
    <>
      {productId === null && <Container maxWidth="sm" component="header" className={classes.header}>
        <Typography component="h1" variant="h2" align="center" gutterBottom>
          Sample Pricing Page
        </Typography>
        <Typography component="p" variant="h5" align="center" color="textSecondary">
          Animated, effective, responsive pricing table for your customers.
        </Typography>
      </Container>}
      <Container maxWidth="md" component="main" className={classes.main}>
        {productId ? getCard(data.find(d => d.id === productId))
          : <Grid container spacing={5} alignItems="flex-end">
            {data.map((tier, index) => <Slide
              key={tier.title}
              direction="up"
              style={{
                transitionDelay: 300 * index,
              }}
              timeout={1000}
              in={showTier === null || showTier === index}>
              <Grid item xs={12} sm={tier.title === 'Product 3' ? 12 : 6} md={4}>
                {getCard(tier, index)}
              </Grid>
            </Slide>)}
          </Grid>}
      </Container>
      {/* <Container maxWidth="md" component="footer" className={classes.footer}>
        <Grid container spacing={4} justify="space-evenly">
          {footers.map(footer => (
            <Grid item xs={6} sm={3} key={footer.title}>
              <Typography variant="h6" color="textPrimary" gutterBottom>
                {footer.title}
              </Typography>
              <ul>
                {footer.description.map(item => (
                  <li key={item}>
                    <Link href="#" variant="subtitle1" color="textSecondary">
                      {item}
                    </Link>
                  </li>
                ))}
              </ul>
            </Grid>
          ))}
        </Grid>
      </Container> */}
    </>
  );
});

export default () => (
  <Match path="/buy/:product">
    {props => <Buy productId={props.match && props.match.product} />}
  </Match>
);
