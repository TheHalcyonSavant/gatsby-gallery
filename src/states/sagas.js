import { eventChannel } from 'redux-saga';
import { call, cancelled, fork, put, select, take } from 'redux-saga/effects';
import { navigate } from 'gatsby';
import { globalHistory } from '@reach/router';
import { getFlags, getStatus, sumObjects } from '@deni-ths/swiss-jobs-lib';
import { readyFirebase } from '../firebase';
import { log, readyNavigate } from '../utils';

const DEFAULT_QUERY = {
  status: 'chosen', // applied, chosen, unread
  keywords: ['React'],
  page: 0,
  pageSize: 10,
};

const getMeta = db => db.collection('meta-swiss-jobs').doc(DEFAULT_QUERY.keywords.join(','));
const putWait = wait => put({ type: 'AUTH_WAIT', auth: { wait } });
const putAlert = (message, variant = 'error') => put({
  type: 'ALERT',
  alert: {
    message,
    open: true,
    variant
  }
});

async function getUserRole(firebaseApp, user) {
  const ref = firebaseApp.firestore().collection('users');
  let role = '';
  for (const p of user.providerData) {
    const fUser = ref.doc(p.uid);
    await fUser.set({
      provider: p.providerId,
      loggedAt: firebaseApp.firebase_.firestore.FieldValue.serverTimestamp(),
      displayName: p.displayName,
      email: p.email,
      emailVerified: user.emailVerified,
      phoneNumber: p.phoneNumber,
      photoURL: p.photoURL,
    }, { merge: true });
    const u = (await fUser.get()).data();
    if (u.role) role = u.role;
  }
  return role;
}

async function getUserMenu(db, user) {
  const userMenu = [];
  const { docs } = await db.collection('pages').get();
  for (const doc of docs) {
    const m = doc.data();
    if (m.role && user.role !== m.role) continue;
    m.deepSlug = m.slug;
    m.slug = `user/${m.slug}`;
    m.menu = (await db.collection(`${doc.ref.path}/menu`).get()).docs[0].data();
    userMenu.push(m);
  }
  return userMenu;
}

async function nav(path) {
  log('saga', 'nav from:', window.location.pathname, 'to:', path);
  return navigate(path, {
    state: {
      saga: true
    },
    replace: true
  });
}

// Use this because gatsby-browser route APIs don't fire on every location change
function* routeChangeListener(db) {
  // Use globalHistory.listen because window popstate listener only fires when "back" and "forward" are clicked
  const chan = yield call(() => eventChannel(emit => globalHistory.listen(({ location }) => emit(location))));
  while (true) {
    const { state } = yield take(chan);
    log(window.location.href, state);
    switch (window.location.pathname) {
      case '/user/jobs': yield call(queryJobs, state, db); break;
      default: yield putWait(false);
    }
  }
}

function* queryJobs(query, db) {
  const usp = new URLSearchParams(window.location.search);
  if (!query.hasOwnProperty('accessDB')) query.accessDB = true;
  usp.set('status', query.status = query.status || usp.get('status') || DEFAULT_QUERY.status);
  usp.set('keywords', query.keywords = query.keywords
    || (usp.has('keywords') ? usp.get('keywords').split(',') : DEFAULT_QUERY.keywords));
  usp.set('page', query.page = query.hasOwnProperty('page')
    ? query.page
    : (usp.has('page') ? (parseInt(usp.get('page')) || 0) : DEFAULT_QUERY.page));
  usp.set('pageSize', query.pageSize = query.hasOwnProperty('pageSize')
    ? query.pageSize
    : (usp.has('pageSize') ? (parseInt(usp.get('pageSize')) || 0) : DEFAULT_QUERY.pageSize));
  window.history[query.pushHistory ? 'replaceState' : 'replaceState'](query, null, `/user/jobs?${usp.toString()}`);
  log(window.location.pathname + window.location.search, query.accessDB, query);

  const auth = yield select(state => state.auth);
  const admin = auth.admin;
  const start = query.page * query.pageSize;
  let allJobs = admin.allJobs, ref;
  if (query.accessDB) {
    ref = getMeta(db);
    const meta = (yield call([ref, ref.get])).data();
    admin.totalCount = meta[query.status];
    admin.jobsMeta = meta;
    admin.initiateQuering = (admin.initiateQuering || 0) + 1;
    allJobs = [];
  }
  let end = Math.min(admin.totalCount, start + query.pageSize);
  if (!query.accessDB) {
    let nulls = 0;
    for (let i = start; i < end; i++) {
      if (allJobs[i] === null) nulls++;
      else if (!allJobs[i]) {
        query.accessDB = true;
        break;
      }
    }
    if (nulls >= (end - start)) query.accessDB = true;
  }
  if (query.accessDB) {
    ref = db.collection('swiss-jobs')
      .where('ours.keywords', 'array-contains', 'React')
      //.where('ours.test', '>', 0)
      //.where('ours.id', '==', '3ffpOQ20hogd6jaNwFiV')
      .where('ours.status', '==', query.status)
      .orderBy('ours.flags.order', 'desc')
      .orderBy('ours.date', 'desc');
    //ref = ref.orderBy('ours.test', 'desc')
    // jobs.sort((a, b) => {
    //   const aOrder = a.ours.order || 0;
    //   const bOrder = b.ours.order || 0;
    //   if (aOrder < bOrder) return 1;
    //   if (aOrder > bOrder) return -1;
    //   if (a.ours.date < b.ours.date) return 1;
    //   if (a.ours.date > b.ours.date) return -1;
    //   return 0;
    // })
    if (allJobs[start - 1]) ref = ref.startAt(allJobs[start - 1].ours.ref);
    ref = ref.limit(query.pageSize + 1);
    // this can be used only when long jump between pages gets available
    let swissJobs = (yield call([ref, ref.get])).docs;
    // slice operation is necessary because firestore startAt is a little weird,
    // it doesn't support numbers, but only real objects
    log(swissJobs.length, start, end, query);
    if (!start) swissJobs = swissJobs.slice(0, query.pageSize);
    else swissJobs = swissJobs.slice(1);
    swissJobs = swissJobs.map(r => {
      const o = r.data();
      o.ours.ref = r;
      return o;
    });
    if (swissJobs.length < end - start) end = admin.totalCount = start + swissJobs.length;
    for (let i = start; i < end; i++) {
      allJobs[i] = swissJobs.shift();
    }
    query.accessDB = false;
  }
  admin.allJobs = allJobs;
  admin.jobs = allJobs.slice(start, end).filter(j => j);
  admin.query = query;
  auth.wait = false;
  yield put({ type: 'ADMIN_JOBS_QUERIED', auth });
}

async function getFetchData(r) {
  if (!r.ok) {
    throw Error(await r.text());
  }
  return r.json();
}

async function fetchFullJobPerSite(headers) {
  const ours = await fetch(`https://us-central1-hazel-cedar-540.cloudfunctions.net/swiss-jobs?f=fetchFullJob`, {
    headers,
  }).then(getFetchData);
  return ours;
}

function* fetchJobs(payload, db) {
  yield putWait(true);
  const token = yield select(state => state.auth.admin.token);
  const ref = getMeta(db);
  let meta = {};
  const webpages = ['job-room.ch', 'jobs.ch'];
  for (const api of webpages) {
    const r = yield call(fetch, `https://us-central1-hazel-cedar-540.cloudfunctions.net/swiss-jobs?f=fetchJobs&api=${api}`, {
      headers: {
        keywords: JSON.stringify(DEFAULT_QUERY.keywords),
        token,
      },
    });
    meta = sumObjects(meta, yield call(getFetchData, r));
  }
  yield call([ref, ref.set], meta);
  yield call(queryJobs, {}, db);
  yield putAlert(`Successfull download of swiss jobs: ${meta.newJob} new, ${meta.merged} merged`, 'success');
}

function* getJobText({ id, externalId }) {
  const admin = yield select(state => state.auth.admin);
  const job = admin.allJobs.find(j => j && j.ours && j.ours.id === id);
  job.ours = { ...job.ours, ...(yield call(fetchFullJobPerSite, { id, externalId })) };
  yield put({ type: 'ADMIN_GOT_JOB_TEXT', auth: { admin } });
}

function sortJobs(direction) {
  return function* sortJobsGenerator({ jobs }, db) {
    const admin = yield select(state => state.auth.admin);
    const b = db.batch();
    let ref = db.collection('swiss-jobs');
    // const inc = db.app.firebase_.firestore.FieldValue.increment;
    admin.allJobs.filter(j => jobs.includes(j)).forEach(job => {
      b.update(ref.doc(job.ours.id), 'ours.flags', job.ours.flags = getFlags(
        job.ours.flags,
        { order: (job.ours.flags.order || 0) + direction },
        admin.jobsMeta
      ));
      b.update(ref.doc(job.ours.id), 'ours.status', getStatus(job.ours.flags));
    });
    yield call([b, b.commit]);
    ref = getMeta(db);
    yield call([ref, ref.set], admin.jobsMeta);
    admin.initiateQuering++; // every time a job.ours is modified and table needs refreshing, increase this counter
    yield put({ type: 'ADMIN_JOBS_SORTED', auth: { admin } });
  }
}

function* ignoreJobs({ jobsToIgnore }, db) {
  let b = db.batch(), ref = db.collection('swiss-jobs');
  const admin = yield select(state => state.auth.admin);
  for (const job of jobsToIgnore) {
    b.update(
      ref.doc(job.ours.id),
      'ours.flags',
      job.ours.flags = getFlags(job.ours.flags, { ignored: true }, admin.jobsMeta)
    );
    b.update(ref.doc(job.ours.id), 'ours.status', getStatus(job.ours.flags));
    admin.allJobs[admin.allJobs.indexOf(job)] = null;
  }
  yield call([b, b.commit]);
  admin.totalCount -= jobsToIgnore.length;
  admin.initiateQuering++;
  ref = getMeta(db);
  yield call([ref, ref.set], admin.jobsMeta);
  yield put({ type: 'ADMIN_JOBS_IGNORED', auth: { admin } });
}

function* changeJobLang({ jobId, futureLang }) {
  const admin = yield select(state => state.auth.admin);
  const job = admin.allJobs.find(j => j && j.ours && j.ours.id === jobId);
  if (!job) return;
  job.ours.currentLang = futureLang;
  admin.initiateQuering++;
  yield put({ type: 'ADMIN_JOB_TRANSLATED', auth: { admin } });
}

function* changeToolbarState({ toolbarState }) {
  const admin = yield select(state => state.auth.admin);
  admin.toolbarState = toolbarState;
  yield put({ type: 'ADMIN_JOBS_TOOLBAR_STATE_CHANGED', auth: { admin } });
}

function* jobApplied({ id }, db) {
  const admin = yield select(state => state.auth.admin);
  const idx = admin.allJobs.findIndex(j => j && j.ours && j.ours.id === id);
  let ref = db.collection('swiss-jobs').doc(id);
  const newFlags = getFlags(
    admin.allJobs[idx].ours.flags,
    { applied: true },
    admin.jobsMeta
  );

  yield call([ref, ref.update], 'ours.flags', newFlags);
  yield call([ref, ref.update], 'ours.status', getStatus(newFlags));
  ref = getMeta(db);
  yield call([ref, ref.set], admin.jobsMeta);
  admin.totalCount--;
  admin.allJobs[idx] = null;
  admin.initiateQuering++;
  yield put({ type: 'ADMIN_JOB_APPLIED_OK', auth: { admin } });
}

function* runner(callFn, db, effects) {
  for (const [action, func] of Object.entries(effects)) {
    yield fork(function* (action, func, db) {
      while (true) {
        const payload = yield take(action);
        try {
          yield callFn(func, payload, db);
        } catch (ex) {
          yield putAlert(ex.message.replace(/\\n/g, '\n').replace(/\\["']/g, ''));
          console.error(ex.message);
        }
      }
    }, action, func, db);
  }
}

function* adminFlow(db) {
  yield fork(runner, fork, db, {
    ADMIN_MOVE_UP_JOBS: sortJobs(1),
    ADMIN_MOVE_DOWN_JOBS: sortJobs(-1),
  });
  yield fork(runner, call, db, {
    ADMIN_FETCH_JOBS: fetchJobs,
    ADMIN_GET_JOB_TEXT: getJobText,
    ADMIN_CHANGE_JOB_LANG: changeJobLang,
    ADMIN_IGNORE_JOBS: ignoreJobs,
    ADMIN_JOB_APPLIED: jobApplied,
    ADMIN_JOBS_TOOLBAR_STATE_CHANGE: changeToolbarState,
  });
}

export function* authFlow() {
  try {
    const firebaseApp = yield call(readyFirebase);
    yield call(readyNavigate);
    const fbAuth = firebaseApp.auth();
    const db = firebaseApp.firestore();
    yield fork(routeChangeListener, db);
    const chan = yield call(() => eventChannel(emit => fbAuth.onAuthStateChanged(user => emit({ user }))));
    while (true) {
      const { user } = yield take(chan);
      log('saga', 'user', user);
      if (user) {
        if (/login/.test(window.location.pathname)) yield call(nav, '/user/profile/');
        yield putWait(true);
        const auth = {
          user,
          loggedIn: true,
          admin: {},
          wait: false,
        };
        let adminTask;
        auth.userRole = user.role = yield call(getUserRole, firebaseApp, user);
        auth.userMenu = yield call(getUserMenu, db, user); // yield call(sleep, 1500);
        if (auth.userRole === 'admin') {
          auth.admin = {
            token: yield call([user, user.getIdToken]),
          };
          auth.wait = true;
          adminTask = yield fork(adminFlow, db);
        }
        yield put({ type: 'AUTH_LOGIN', auth });
        yield call(queryJobs, {}, db);

        yield take('USER_SIGNOUT');
        yield putWait(true);
        yield call([fbAuth, fbAuth.signOut]); // yield call(sleep, 1500);
        if (auth.userRole === 'admin') {
          adminTask.cancel();
        }
        log('saga', 'signed out');
        continue;
      }
      // yield call(sleep, 1000);
      if (/^\/user\b/.test(window.location.pathname)) yield call(nav, '/login/');
      log('saga', 'will clear');
      yield put({ type: 'AUTH_CLEAR' });
    }
  } catch(ex) {
    console.error(ex);
    yield putAlert(ex.message);
  }
  finally {
    if (yield cancelled()) {
      console.error('cancelled');
      yield put({type: 'AUTH_CANCELLED'})
    }
  }
}
