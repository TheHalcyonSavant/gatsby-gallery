const initialState = {
  auth: {
    admin: {},
    loggedIn: false,
    user: {},
    userMenu: [],
    userRole: '',
    wait: false,
  },
  alert: {}
};

export function alertReducer(state = initialState.alert, { type, alert }) {
  if (type !== 'ALERT') return state;
  return { ...state, ...alert };
}

export function authReducer(state = initialState.auth, action) {
  const { type, auth } = action;

  if (type === 'AUTH_CLEAR') return initialState.auth;
  return { ...state, ...auth }
}
