import { applyMiddleware, compose, createStore, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import createSagaMiddleware from 'redux-saga';
import { alertReducer, authReducer } from './reducers';
import { authFlow } from './sagas';

function configureStore() {
  const sagaMiddleware = createSagaMiddleware()
  const composeArgs = [
    applyMiddleware(sagaMiddleware)
  ];
  // if(window && window.__REDUX_DEVTOOLS_EXTENSION__) {
  //   composeArgs.push(window.__REDUX_DEVTOOLS_EXTENSION__());
  // }

  const store = createStore(
    combineReducers({
      alert: alertReducer,
      auth: authReducer,
      form: formReducer,
      lastType: (state, { type }) => type,
    }),
    compose.apply(undefined, composeArgs)
  );

  sagaMiddleware.run(authFlow);

  return store;
}

export default configureStore();
