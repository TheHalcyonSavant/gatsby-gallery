import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import { sleep } from './utils';

export let firebaseApp;

const timer = setInterval(() => {
  if (!firebase || !firebase.apps || !firebase.auth || !firebase.firestore || !firebase.initializeApp) return;
  clearInterval(timer);
  firebaseApp = firebase.apps.length ? firebase.apps[0] : firebase.initializeApp({
    // cSpell:disable
    apiKey: "AIzaSyAElZEHF7S3MH0XiJS_H6T44bt7Cnq2_AI",
    authDomain: "hazel-cedar-540.firebaseapp.com",
    databaseURL: "https://hazel-cedar-540.firebaseio.com",
    projectId: "hazel-cedar-540",
    storageBucket: "hazel-cedar-540.appspot.com",
    messagingSenderId: "446067840392"
    // cSpell:enable
  });
}, 10);

export const signInOptions = [
  // firebase.auth is undefined on gatsby build
  'google.com',   // firebase.auth.GoogleAuthProvider.PROVIDER_ID
  'facebook.com', // firebase.auth.FacebookAuthProvider.PROVIDER_ID
  'twitter.com',  // firebase.auth.TwitterAuthProvider.PROVIDER_ID
  'github.com',   // firebase.auth.GithubAuthProvider.PROVIDER_ID
  'password',     // firebase.auth.EmailAuthProvider.PROVIDER_ID
  'anonymous'
];

export async function readyFirebase() {
  while (!firebaseApp || !firebaseApp.auth) await sleep(10);
  return firebaseApp;
}
