import { red, grey } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core/styles';

const primaryColor = '#143131';
const spacing = 8;

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: primaryColor,
      // main: teal[900], // MUI colors are very restrictive, so I have to use custom ones
    },
    secondary: {
      main: '#16708a',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: grey[50],
    },
    // type: 'dark' //TODO: why switching to this doesn't work?
  },
  spacing,
  overrides: {
    MuiIconButton: {
      root: {
        color: grey[50],
        '&:hover': {
          backgroundColor: 'rgba(12, 25, 27, 0.7)'
        }
      },
    },
    MuiLink: {
      root: {
        backgroundColor: 'rgba(20, 49, 49, 0.75)',
        color: grey[50],
        cursor: 'pointer',
      }
    },
    MuiListItem: {
      root: {
        color: grey[50],
      },
      button: {
        '&:hover': {
          backgroundColor: primaryColor,
        },
        '&$selected': {
          backgroundColor: 'rgba(12, 25, 27, 0.7)',
        },
        '&$selected:hover': {
          backgroundColor: primaryColor,
        }
      }
    },
    MuiListItemIcon: {
      root: {
        color: grey[50],
      }
    },
    MuiPaper: {
      root: {
        backgroundColor: 'rgba(20, 49, 49, 0.7)',
        color: grey[50],
      }
    },
    MuiTypography: {
      h3: {
        margin: '1rem 0 1em',
      },
      colorTextSecondary: {
        color: 'auto'
      }
    },
  },
});
