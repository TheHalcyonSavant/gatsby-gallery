import React from 'react';
import { Link } from 'gatsby';
import MuiLink from '@material-ui/core/Link';

export default ({ ...rest }) => {
  return <MuiLink component={Link} {...rest} />;
};
