import React from 'react';
import { connect } from 'react-redux';
import {
  IconButton,
  Slide,
  Snackbar,
} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import {
  CheckCircle as CheckCircleIcon,
  Close as CloseIcon,
  Error as ErrorIcon,
  Info as InfoIcon,
  Warning as WarningIcon,
} from '@material-ui/icons'; 
import styles from './Alert.css';

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

export default connect(state => ({
  message: state.alert.message,
  open: state.alert.open,
  variant: state.alert.variant,
}), {
  dismiss: () => ({ type: 'ALERT', alert: { open: false }})
})(withStyles(styles)(({ classes, dismiss, message, open, variant }) => {
  const Icon = variantIcon[variant];
  return (<Snackbar
    className={classes.root}
    aria-describedby={classes.message}
    open={open}
    anchorOrigin={{
      vertical: 'top',
      horizontal: 'center'
    }}
    ContentProps={{ className: `${classes.subRoot} ${classes[variant]}` }}
    message={variant && (<pre id={classes.message} className={classes.message}>
      <Icon className={classes.leftIcon} />{message}
    </pre>)}
    TransitionComponent={Slide}
    TransitionProps={{ direction: 'down' }}
    action={<IconButton
      aria-label="close"
      color="inherit"
      className={classes.closeIcon}
      onClick={dismiss}>
      <CloseIcon />
    </IconButton>}
  />);
}));
