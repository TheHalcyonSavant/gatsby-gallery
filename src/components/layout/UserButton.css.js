export default theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight: theme.spacing(2),
    minWidth: theme.spacing(7),
    '&.loggedOut': {
      [theme.breakpoints.up('sm')]: {
        minWidth: theme.spacing(15),
      }
    },
    '& h6': {
      marginLeft: theme.spacing(1),
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      }
    },
  },
  button: {
    padding: theme.spacing(2),
    borderRadius: '50%',
    '.loggedOut &': {
      [theme.breakpoints.up('sm')]: {
        borderRadius: '16%',
      }
    }
  },
  progress: {
    color: 'white',
  },
  menu: {
    '& .MuiMenuItem-root svg': {
      marginRight: theme.spacing(1)
    },
  },
});
