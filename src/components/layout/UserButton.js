import React from 'react';
import { connect } from 'react-redux';
import { navigate } from 'gatsby';
import { withStyles } from '@material-ui/core/styles';
import {
  CircularProgress,
  IconButton,
  MenuItem,
  MenuList,
  Popover,
  Typography,
} from '@material-ui/core';
import * as icons from '@material-ui/icons';
import { actions } from '../../states/connectArgs';
import { log } from '../../utils';
import styles from './UserButton.css.js';

export default connect(state => ({
  visibility: state.auth.wait || (typeof window !== 'undefined' && /\blogin\b/.test(window.location.pathname) && 'hidden'),
  loggedIn: state.auth.loggedIn,
  wait: state.auth.wait,
}), actions)(withStyles(styles)(({ classes, loggedIn, logOut, wait, visibility }) => {
  log('UserButton', `loggedIn = ${loggedIn}`, `wait = ${wait}`);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  function handleClick(event) {
    if (!loggedIn) {
      navigate('/login/');
      return;
    }
    setAnchorEl(event.currentTarget);
  }
  function handleClose() {
    setAnchorEl(null);
  }
  function handleProfile() {
    navigate('/user/profile/');
    handleClose();
  }
  function handleLogout() {
    logOut();
    handleClose();
  }
  return (
    <div className={`${classes.root} logged${loggedIn ? 'In' : 'Out'}`} style={{ visibility }}>
      {wait && <CircularProgress
        className={classes.progress}
        size={28}
        thickness={5} />}
      {!wait && <IconButton 
        aria-label="User"
        aria-owns={open ? 'menu-list-grow' : undefined}
        aria-haspopup="true"
        className={classes.button}
        onClick={handleClick}>
        <icons.Person />
        {!loggedIn && <Typography variant="h6">Login</Typography>}
      </IconButton>}
      <Popover
        id="menu-list-grow"
        className={classes.menu}
        open={open}
        onClose={handleClose}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}>
        <MenuList>
          <MenuItem onClick={handleProfile}>
            <icons.Pageview />
            Profile
          </MenuItem>
          <MenuItem onClick={handleClose}>
            <icons.Security />
            My account
          </MenuItem>
          <MenuItem onClick={handleLogout}>
            <icons.PowerSettingsNew />
            Logout
          </MenuItem>
        </MenuList>
      </Popover>
    </div>
  )
}));
