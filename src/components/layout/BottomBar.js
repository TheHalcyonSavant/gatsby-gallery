import React from 'react';
import { Container, List, ListItem, withTheme } from '@material-ui/core';
import Link from '../Link';

export default withTheme(({ theme }) => (
  <div style={{ backgroundColor: theme.palette.primary.main }}>
    <Container style={{ padding: theme.spacing(1, 2) }}>
      <List style={{ display: 'flex', flexDirection: 'row' }}>
        <ListItem>
          <Link to="/terms-of-service">
            Terms Of Service
          </Link>
        </ListItem>
        <ListItem>
          <Link to="/privacy-policy">
            Privacy Policy
          </Link>
        </ListItem >
        <ListItem style={{ justifyContent: 'flex-end' }}>
          Deni Kotsev 2019 &reg;
        </ListItem>
      </List>
    </Container>
  </div>
));
