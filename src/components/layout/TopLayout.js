import React from 'react';
import Helmet from 'react-helmet';
import { Provider } from 'react-redux';
import { graphql, StaticQuery } from 'gatsby';
import { CssBaseline, Container } from '@material-ui/core';
import { ThemeProvider, withStyles } from '@material-ui/styles';
import store from '../../states/store';
import { theme } from '../../theme';
import Bar from './Bar';
import BottomBar from './BottomBar';
import globalStyles from './global.css.js';
import './TopLayout.module.css';

const Root = withStyles(globalStyles)(({ element, ...rest }) => (
  <div style={{ flexGrow: 1 }}>
    <StaticQuery
      query={graphql`
        query BlogPages {
          blog {
            pages {
              slug
              title
              description
              menu {
                name
                muiIcon
                order
              }
            }
          }
          allMarkdownRemark {
            edges {
              node {
                fields {
                  slug
                }
                frontmatter {
                  title
                }
              }
            }
          }
        }
      `}
      render={data => <Bar
        pages={data.blog.pages}
        markdowns={data.allMarkdownRemark.edges}
        currentPath={rest.props.path.replace(/\/|\*/g, '')}
      />}
    />
    <Container maxWidth={false} style={{
      padding: 0, // TODO: wait for #15708 to close
      minHeight: 'calc(100vh - 132px)', // 132 = top bar height + bottom bar height
    }} className="rootcontainer">
      {element}
    </Container>
    <BottomBar />
  </div>
));

export const reduxStore = store;

export const TopLayout = ({ ...props }) => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <Helmet htmlAttributes={{ lang: 'en' }}>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
        />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
        <meta name="msapplication-TileColor" content="#00aba9" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet" />

        {/* <script src="/__/firebase/6.0.1/firebase-app.js" />
        <script src="/__/firebase/6.0.1/firebase-auth.js" />
        <script src="/__/firebase/6.0.1/firebase-firestore.js" /> */}
      </Helmet>
      <CssBaseline />
      <Root {...props} />
    </ThemeProvider>
  </Provider>
);
