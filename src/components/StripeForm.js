import React, { useEffect, useState } from 'react';
import Cards from 'react-credit-cards';
import 'react-credit-cards/es/styles-compiled.css';
import {
  CardCvcElement,
  CardExpiryElement,
  CardNumberElement,
  Elements,
  injectStripe,
  StripeProvider,
} from 'react-stripe-elements';
import { Button } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import clsx from 'clsx';
import styles from './StripeForm.css';

const iframeStyles = (fontSize = '14px', padding) => ({
  style: {
    base: {
      fontSize,
      color: '#424770',
      letterSpacing: '0.025em',
      fontFamily: 'Source Code Pro, monospace',
      '::placeholder': {
        color: '#aab7c4',
      },
      padding,
    },
    invalid: {
      color: '#9e2146',
    },
  }
});

const Form = withStyles(styles)(injectStripe(({ classes, className, onPay, stripe, style }) => {
  const [focus, setFocus] = useState('number');
  const [brand, setBrand] = useState('unknown');
  const getCardField = v => v.elementType.substr(4).toLowerCase();
  function handleFocus(v) {
    setFocus(getCardField(v));
  }
  function handleChange(v) {
    setBrand(v.brand);
  }
  function handleSubmit(ev) {
    ev.preventDefault();
    stripe.createToken().then(payload => onPay && onPay(payload));
  }
  return (
    <form
      className={clsx(classes.form, className)}
      style={{ ...style }}
      onSubmit={handleSubmit}>
      <Cards
        number={''}
        name={''}
        expiry={''}
        cvc={''}
        focused={focus}
        preview={true}
        issuer={brand}
        placeholders={{ name: '' }}
      />
      <CardNumberElement
        onFocus={handleFocus}
        onChange={handleChange}
        {...iframeStyles()}
      />
      <div className={classes.inline}>
        <CardExpiryElement
          onFocus={handleFocus}
          {...iframeStyles()}
          className={classes.leftInline}
        />
        <CardCvcElement
          onFocus={handleFocus}
          {...iframeStyles()}
        />
      </div>
      <Button style={{ alignSelf: 'flex-end' }} type="submit" variant="contained">Pay</Button>
    </form>
  );
}));

export default function({ ...rest }) {
  const [stripe, setStripe] = useState(null);
  const onload = () => {
    setStripe(window.Stripe(process.env.GATSBY_STRIPE_KEY));
  };
  useEffect(() => {
    let script = document.querySelector('#stripe-api-script');
    if (script) {
      if (!stripe) onload();
      return;
    }
    script = document.createElement('script');
    script.onload = onload;
    script.id = 'stripe-api-script';
    script.src = 'https://js.stripe.com/v3/';
    document.body.appendChild(script);
  });
  if (!stripe) return null;
  return (
    <StripeProvider stripe={stripe}>
      <Elements>
        <Form {...rest} />
      </Elements>
    </StripeProvider>
  );
}
