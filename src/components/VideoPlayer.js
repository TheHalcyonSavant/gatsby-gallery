import React from 'react';
import ReactPlayer from 'react-player'
import { withStyles } from '@material-ui/styles';
import styles from './VideoPlayer.css';

export default withStyles(styles)(({ classes, url, ...props }) => {
  const q = typeof window !== 'undefined' ? `?origin=${window.location.origin}` : '';
  // Responsive ReactPlayer
  return (
    <div className={classes.wrapper}>
      <ReactPlayer className={classes.player} width='100%' height='100%' url={`${url + q}`} {...props} />
    </div>
  );
});
