export default theme => ({
  form: {
    margin: '0 auto',
    fontSize: '18px',
    fontFamily: 'Helvetica Neue, Helvetica, Arial, sans-serif',
    maxWidth: '500px',
    overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
    padding: `${theme.spacing(1)}px 0`,

    '& label': {
      color: '#6b7c93',
      fontWeight: 300,
      letterSpacing: '0.025em',
    },

    '& input, & .StripeElement': {
      display: 'block',
      marginTop: '8px',
      marginBottom: '16px',
      padding: '10px 14px',
      fontSize: '1em',
      fontFamily: 'Source Code Pro, monospace',
      boxShadow: 'rgba(50, 50, 93, 0.14902) 0px 1px 3px, rgba(0, 0, 0, 0.0196078) 0px 1px 0px',
      border: 0,
      outline: 0,
      borderRadius: '4px',
      background: 'white',
    },

    '& input::placeholder': {
      color: '#aab7c4',
    },

    '& input:focus, & .StripeElement--focus': {
      boxShadow: 'rgba(50, 50, 93, 0.109804) 0px 4px 6px, rgba(0, 0, 0, 0.0784314) 0px 1px 3px',
      transition: 'all 150ms ease',
    },

    '& button': {
      whiteSpace: 'nowrap',
      border: 0,
      outline: 0,
      display: 'inline-block',
      height: '40px',
      lineHeight: '40px',
      padding: '0 14px',
      boxShadow: '0 4px 6px rgba(50, 50, 93, .11), 0 1px 3px rgba(0, 0, 0, .08)',
      color: '#fff',
      borderRadius: '4px',
      fontSize: '15px',
      fontWeight: 600,
      textTransform: 'uppercase',
      letterSpacing: '0.025em',
      backgroundColor: '#6772e5',
      textDecoration: 'none',
      transition: 'all 150ms ease',
      marginTop: '10px',
    },
    '& button:hover': {
      color: '#fff',
      cursor: 'pointer',
      backgroundColor: '#7795f8',
      transform: 'translateY(-1px)',
      boxShadow: '0 7px 14px rgba(50, 50, 93, .10), 0 3px 6px rgba(0, 0, 0, .08)',
    }
  },
  inline: {
    display: 'flex',
    flexDirection: 'row',

    '& > *': {
      flexGrow: 1
    }
  },
  leftInline: {
    marginRight: '10%',
  }
});
