import { amber, green } from '@material-ui/core/colors';

export default theme => ({
  root: {
    flexWrap: 'nowrap',
    '& .MuiSvgIcon-root': {
      fontSize: theme.spacing(2.5),
    },
    '& .MuiIconButton-root:hover': {
      backgroundColor: theme.palette.action.hover
    }
  },
  subRoot: {
    flexWrap: 'unset',
  },
  message: {
    display: 'flex',
    alignItems: 'center',
    fontFamily: theme.typography.fontFamily,
    fontSize: theme.typography.htmlFontSize,
    margin: 0,
    wordBreak: 'break-all',
    whiteSpace: 'pre-wrap',
  },
  leftIcon: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  closeIcon: {
    padding: theme.spacing(0.5)
  },
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.main,
  },
  warning: {
    backgroundColor: amber[700],
  },
});
