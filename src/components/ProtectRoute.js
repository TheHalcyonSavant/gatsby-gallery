import React from 'react';
import { connect } from 'react-redux';
import Loading from '../components/Loading';
import { log } from '../utils';

export default (role = 'any') => SubComponent => connect(state => ({
  loggedIn: state.auth.loggedIn,
  isAdmin: state.auth.userRole === 'admin',
}))(props => {
  const { loggedIn, isAdmin } = props;
  if (!loggedIn) {
    log('ProtectRoute', 'loading...');
    // waitCountdown(); // TODO: I don't know why I've invented this
    return <Loading />;
  }
  if (role === 'admin' && !isAdmin) {
    log('isAdmin', isAdmin);
    return <h1>Permission denied</h1>
  }
  log('ProtectRoute', 'pass');
  return <SubComponent />;
});
