export default theme => ({
  header: {
    ...theme.typography.h3,
    textAlign: 'center',
    margin: '1em 0',
    display: 'block',
    color: 'inherit',
    background: 'inherit',
  },
  paragraph: {
    ...theme.typography.h6,
    fontWeight: 400,
    margin: theme.spacing(1, 0),
    textIndent: theme.spacing(6),
  },
  card: {
    float: 'right',
    textAlign: 'center',
    margin: theme.spacing(2),
    [theme.breakpoints.down('lg')]: {
      display: 'none',
    }
  },
  cardHeader: {
    paddingBottom: 0,
  },
  cardContent: {
    '&:last-child': {
      paddingTop: 0,
      paddingBottom: 0,
    },
    '& .gatsby-image-wrapper': {
      display: 'block !important',
      margin: 'auto',
    },
  },
  contactDetails: {
    width: 200,
    fontSize: 14,
    fontFamily: 'Arial Narrow',
    '& .MuiGrid-item': {
      display: 'flex',
      alignItems: 'center',
    },
    '& .MuiGrid-grid-xs-10': {
      justifyContent: 'flex-end',
    },
  },
  newBadge: {
    display: 'inline-block',
    width: '50px',
    height: '38px',
    verticalAlign: 'top',
    paddingTop: '14px',
    transform: 'rotate(-45deg)',
    fontSize: '11px',
    fontWeight: 700,
    color: 'white',
  },
});
