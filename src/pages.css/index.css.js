export default theme => ({
  parallaxFrame: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    display: 'flex',
    flexFlow: 'row wrap',
    alignItems: 'center',
    justifyContent: 'center',
    '& h1': {
      color: 'white',
      textShadow: '3px 5px 13px #ccc',
      textAlign: 'center',
    }
  },
  imageText: {
    fontSize: '8vh',
    [theme.breakpoints.up('md')]: {
      fontSize: '6vw',
    },
  },
  parallaxVideo: {
    height: '65vh !important',
    [theme.breakpoints.up('md')]: {
      height: '20vh !important',
      '& h1': {
        lineHeight: 0,
      },
    },
    '& video': {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    }
  },
  videoText: {
    fontSize: '10vh',
  },
});
