import grey from '@material-ui/core/colors/grey'

export default theme => ({
  '@global': {
    html: {
      scrollBehavior: 'smooth'
    },
  },
  main: {
    display: 'flex',
    justifyContent: 'center',
    '& .MuiGrid-container': {
      justifyContent: 'center'
    },
    '& .MuiCard-root': {
      overflow: 'visible',
      transition: 'all 1s ease-out .5s',
    },
    '& .MuiCardContent-root': {
      color: grey[900],
      padding: theme.spacing(2),
    },
    '& .MuiCardActions-root': {
      overflow: 'hidden',
      transition: 'all 1s',
    },
    '& .MuiCircularProgress-root': {
      color: 'white',
    }
  },
  header: {
    padding: theme.spacing(8, 0, 6),
    transition: 'all 1s'
  },
  cardHeader: {
    backgroundColor: theme.palette.secondary.main,
  },
  price: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    height: theme.spacing(11),
  },
  detachedPrice: {
    position: 'absolute',
    top: 0,
    left: 'calc(50% - 50px)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '50%',
    transition: 'all 1s',
    height: '88px',
    width: '88px',
    opacity: 0,
    zIndex: 2,
    color: grey[50],
    '& > *': {
      transition: 'all 1s',
    },
    '.picked &': {
      opacity: 1,
      backgroundColor: theme.palette.secondary.main,
      left: '-15%',
      boxShadow: '1px 2px 5px #000',
      height: '88px',
      width: '88px',
      '& > *': {
        fontSize: `1.2rem`,
      },
      [theme.breakpoints.down('sm')]: {
        left: '-5%',
      }
    },
    '.finished &': {
      backgroundColor: '#597e1b'
    }
  },
  staticPrice: {
    display: 'flex',
    alignItems: 'center',
    '.picked &': {
      animation: '$staticPriceMove 1s forwards',
    }
  },
  waitOverlay: {
    position: 'fixed',
    backgroundColor: 'rgba(255,255,255,0.28)',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    zIndex: 1,
  },
  '@keyframes staticPriceMove': {
    '0%': {
      opacity: 0,
    },
    '50%': {
      opacity: 0,
      marginLeft: 0,
    },
    '51': {
      opacity: 1,
    },
    '100%': {
      marginLeft: theme.spacing(5),
    }
  },
  form: {
    padding: 0,
    overflow: 'hidden',
    animation: '$formExpand 2s forwards',
    // height: 0,
  },
  '@keyframes formExpand': {
    '0%': {
      height: 0,
    },
    '100%': {
      height: '360px',
    }
  },
  // footer: {
  //   borderTop: `1px solid ${theme.palette.divider}`,
  //   marginTop: theme.spacing(8),
  //   paddingTop: theme.spacing(3),
  //   paddingBottom: theme.spacing(3),
  //   [theme.breakpoints.up('sm')]: {
  //     paddingTop: theme.spacing(6),
  //     paddingBottom: theme.spacing(6),
  //   },
  // },
});
