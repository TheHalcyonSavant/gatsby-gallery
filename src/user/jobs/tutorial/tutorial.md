![swiss flag]
Are you a foreigner like me who's looking for work in Switzerland that best fits your skills?
Well, here are the problems that you're going to face.
The first problem is that you're going to stare confused too many times at similar job posts on different websites as you're
trying to figure out whether they're posted by the same company or wonder if you have applied already.
![all]

The second problem is that when some job contains some of your desired skills, can also require skills that you don't posses,
so there's no easy way to blacklist them. Therefore, you spend unnecessary time and energy in reading them.
![jobscout24.ch] - For example, when I search for all the job posts containing the React keyword, I get the following ones:
                   How can I blacklist posts that have both "SpringBoot" and "JavaEE" keywords?
Also, you're forced to ignore recommended or featured job posts placed as ads that don't usually correspond to your skills or
you aren't able to filter out old posts that you've already read the other day.

The third problem is that none of these websites allows you to order by the jobs you personally like the most and put them at higher priority before applying.

Lastly, most of the hiring companies expect direct contact with job seekers and not with recruiters. So you're left alone with all these problems.
![? not necessary]
In other words, none of these websites care about your personal organization when searching and applying for jobs, you have to do it by yourself in a painful manner.

Or you can use the SJS webapp. I've created SJS to solve most of these problems, and it's designed from the ground up, prepared to anticipate future problems.

With SJS you can save a lot of clicks and time while navigating over different UIs. All job posts are displayed in just one place,
rather then being bombarded by irrelevant content such as visiting different job sites, agencies, emails, social networks feeds, and so on.
Also, SJS is ad free. You only need to get email authorization to access it.

Now let's see how it works.

After you log in with your authorized email, visit the Jobs link from the menu on the left.
You can see that all swiss jobs are ordered in one big table which displays 10 rows per page.
Every available job is recognized by their title, company and location, as well as their brief descriptions without any additional clicks.
Your searched keyword is highlighted in yellow in the titles and the descriptions.
Also, every job can have certain minimal language requirement (for example, this one requires middle level German and English skills).
In this column, you can see the source from where the job post arrived in here.
It's very important to know when this job was posted, and you can easily sort them by clicking the date column - from newer to older and vice versa.
You can also sort other columns, which gives you ultimate inside when comparing all the content posted by hiring companies.
The last column "Priority", shows the level of personal importance of this job to you.
For example, if you like some jobs more and you would like to apply to them sooner than the others, then you can just assign higher priority,
by selecting the jobs you find interesting and then click the upper arrow button at the top.
Later, you can sort by priority your most valuable job offers that you've marked earlier.
Also, one of the most important feature in SJS webapp is that you can hide jobs that you don't want to see them again,
by selecting the job rows and then click on the hide button located at the upper right corner of the screen.
In my case, the keyword React is highlighted, so that I can easily notice my searched skills over every job offer fields.
If you want to see the full job offer, just click on the first row button.
The job fields and the job full description are translated automatically on every collapse.
You can notice the original language from which the full description was translated at the right corner here.
When you click it it will translate the job to the original language. Click it again, and you will see it in English.
This way, you can compare and find very quickly any badly translated text if you're somewhat familiar with the original language.
In the description, you can easily notice the unwanted keywords. In my case, the unwanted keyword "German" is highlighted here.
This way, I can quickly decide to ignore this job post and go through the entire job seeking process much quicker than what the other platforms offer.
At this moment, the unwanted keywords are hardcoded in the webapp.
However, you can easily type them at the top in the future, the same way like the wanted keywords just in a different input field.

And that's it. Now you know how to use SJS app for new job offers much easier than before.
