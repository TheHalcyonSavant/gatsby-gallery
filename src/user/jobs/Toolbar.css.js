export default theme => ({
  root: {
    '& [class*="MuiGrid-item"]': {
      paddingLeft: theme.spacing(2),
      wordBreak: 'break-all',
    }
  },
  detachedToolbar: {
    [theme.breakpoints.up('sm')]: {
      position: 'fixed',
      left: 0,
      width: '100%',
      zIndex: 100,
      backgroundColor: 'white',
      border: '1px solid #dbdbdb',
      top: theme.spacing(8),
      '& + div': {
        marginTop: theme.spacing(14),
      },
    },
  },
  flags: {
    color: theme.palette.text.secondary,
    '& [class*=MuiGrid-item]': {
      textAlign: 'right',
      paddingLeft: '0 !important',
    }
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
  },
  priority: {
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
    textAlign: 'center',
  }
});
