export default theme => ({
  details: {
    margin: 0,
    padding: theme.spacing(3),
    position: 'relative',
    // whiteSpace: 'pre-wrap',
  },
  iframe: {
    width: '100%',
    border: 0,
  },
  translateButton: {
    position: 'absolute',
    right: theme.spacing(3),
  },
  progress: {
    margin: theme.spacing(3),
    marginLeft: theme.spacing(8)
  },
});
