import React, { memo, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import debounce from 'lodash/debounce';
import {
  Checkbox,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
} from '@material-ui/core';
import { MTableToolbar } from 'material-table';
import withStyles from '@material-ui/core/styles/withStyles';
import { changeUrl } from '../../utils';
import styles from './Toolbar.css';

const ToolbarContent = connect(({ auth: { admin: { jobsMeta, query } }}) => ({
  status: query.status,
  jobsMeta,
  isChanged: jobsMeta && (jobsMeta.chosen + jobsMeta.ignored),
}), {
  changeToolbarState: toolbarState => ({ type: 'ADMIN_JOBS_TOOLBAR_STATE_CHANGE', toolbarState }),
})(withStyles(styles)(({ classes, status, jobsMeta, changeToolbarState }) => { // isChanged
  function onChangeStatus({ target: { value }}) {
    changeUrl({ status: value, page: 0, pushHistory: true });
    changeToolbarState({ enabled: false });
  }
  // const isIgnored = status === 'unread' && jobsMeta && jobsMeta.ignored;
  // console.log('%cRender ToolbarContent', 'color: #56a831', isIgnored);
  // useEffect(() => {
  //   //console.log(isIgnored);
  //   // optimization TODO: why onSelectionChange is fired twice?
  //   return;
  //   if (!isIgnored || !document.querySelector('#autoExpandNextJob').checked) return;
  //   const tableHead = document.querySelector('[class^=MuiTableHead-root]');
  //   if (tableHead.querySelector('[class*="MuiCheckbox-indeterminate"]') || tableHead.querySelector('[class*="Mui-checked"]')) return;
  //   const observer = new MutationObserver(mutationsList => {
  //     if (!mutationsList[0].target || !mutationsList[0].target.children.length) return;
  //     const trs = mutationsList[0].target.children;
  //     if (!trs.length) return;
  //     let nextUnorderedRow;
  //     for (let tr of trs) {
  //       if (tr.querySelector('[class*="priorityColumn"]').textContent === '0') {
  //         nextUnorderedRow = tr;
  //         break;
  //       }
  //     }
  //     // document.querySelector('#toolbar input[placeholder="Search"]').value = 'React';
  //     const nextExpandButton = document.querySelectorAll('.centered-container button[style*="transform"]')[parseInt(nextUnorderedRow.getAttribute('path'))];
  //     if (!nextExpandButton) return;
  //     nextExpandButton.click();
  //     observer.disconnect();
  //   });
  //   observer.observe(document.querySelector('tbody[class^="MuiTableBody"]'), { childList: true });
  //   return () => observer.disconnect();
  // }, [isIgnored]);
  return <Grid container alignItems="center">
    <Grid item lg={4} sm={6} className={classes.flags}>
      {jobsMeta && <Grid container>
        <Grid item lg={3} xs={6}>
          {jobsMeta.available} available<br/>{jobsMeta.removed} removed
        </Grid>
        <Grid item lg={3} xs={6}>
          {jobsMeta.newJob} new jobs<br/>{jobsMeta.merged} merged
        </Grid>
        <Grid item lg={3} xs={6}>
          {jobsMeta.applied} applied<br/>{jobsMeta.ignored} ignored
        </Grid>
        <Grid item lg={3} xs={6}>
          {jobsMeta.chosen} chosen<br/>{jobsMeta.unread} unread
        </Grid>
      </Grid>}
    </Grid>
    <Grid item lg={4} sm={6}>
      <RadioGroup
        aria-label="status"
        row
        className={classes.center}
        name="status"
        value={status}
        onChange={onChangeStatus}>
        <FormControlLabel
          control={<Radio />}
          labelPlacement="end"
          label="Applied"
          value="applied"
        />
        <FormControlLabel
          control={<Radio />}
          labelPlacement="end"
          label="Chosen"
          value="chosen"
        />
        <FormControlLabel
          control={<Radio />}
          labelPlacement="end"
          label="Unread"
          value="unread"
        />
      </RadioGroup>
    </Grid>
    <Grid item lg={4} sm={12} className={classes.center}>
      <FormControlLabel
        control={<Checkbox id="autoExpandNextJob" checked />}
        label="Auto-expand next job when single job gets hidden"
      />
    </Grid>
  </Grid>
}));

const ToolbarEvents = connect(({ auth: { admin } }) => ({
  enabled: Boolean(admin.toolbarState && admin.toolbarState.enabled)
}))(withStyles(styles)(({ classes, enabled }) => {
  const domEls = useRef(null);
  useEffect(() => {
    console.log('%cToolbarEvents useEffect', 'color: #2786ad', enabled, domEls.current);
    if (!domEls.current) {
      domEls.current = {
        toolbar: document.querySelector('#toolbar'),
        onScrollWindow: () => {
          const winScroll = document.body.scrollTop || document.documentElement.scrollTop;
          // const height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
          // const scrolled = winScroll / height;
          const newDetachedState = winScroll > 18;
          if (domEls.current.oldDetachedState !== newDetachedState) {
            // console.log('scroll?', domEls.current.oldDetachedState, newDetachedState);
            domEls.current.toolbar.classList.toggle(classes.detachedToolbar, newDetachedState);
            domEls.current.oldDetachedState = newDetachedState;
          }
        }
      };
      domEls.current.handler = debounce(domEls.current.onScrollWindow, 60);
    }
    domEls.current.oldDetachedState = false;
    if (enabled) {
      // console.log('handler added');
      window.addEventListener('scroll', domEls.current.handler);
      // domEls.current.handler();
    } else {
      // console.log('remove handler');
      window.removeEventListener('scroll', domEls.current.handler);
      domEls.current.handler.cancel();
      domEls.current.toolbar.classList.remove(classes.detachedToolbar);
    }
  }, [enabled, classes.detachedToolbar]);
  return null;
}));

const WrappedToolbar = connect(({ lastType }) => ({
  areJobsSorted: lastType === 'ADMIN_JOBS_SORTED',
}))(withStyles(styles)(({
  classes, passedProps
}) => {
  // console.log('%cRender Toolbar', 'color: #140a84', passedProps);
  return (
    <div id='toolbar' className={classes.root}>
      <MTableToolbar {...passedProps} />
      {/* ToolbarEvents and ToolbarContent should not re-render unnecessary */}
      <ToolbarEvents />
      <ToolbarContent />
      {passedProps.selectedRows.length > 0 && <div className={classes.priority}>
        {passedProps.selectedRows.length > 1 ? 'Priorities' : 'Priority'} =&nbsp;
        {passedProps.selectedRows.map(j => j.ours.flags.order).join(',')}
      </div>}
    </div>
  );
}));

export default memo(props => {
  // console.log('%cRender components', 'color: #c71547de');
  return <WrappedToolbar passedProps={props} />
}, (prevProps, nextProps) => {
  return !nextProps.data.length || ((prevProps.data.length === nextProps.data.length)
    && (prevProps.selectedRows.length === nextProps.selectedRows.length))
});
