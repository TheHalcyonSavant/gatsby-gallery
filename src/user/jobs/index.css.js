import { yellow, orange } from '@material-ui/core/colors';

export const constStyles = {
  selectedRowColor: 'rgb(226, 237, 240)',
  highlightedGoodKeywordColor: yellow.A400,
  highlightedBadKeywordColor: orange.A200,
};

export const tableStyles = {
  MuiTableRow: {
    root: {
      '&:hover': {
        backgroundColor: constStyles.selectedRowColor
      },
    },
  },
};

export default theme => ({
  title: {
    padding: '8px 16px',
    background: 'red',
    color: 'white',
    fontSize: '1.5rem',
    fontWeight: 700,
    fontFamily: 'Arial Black',
  },
  table: {
    marginTop: theme.spacing(2),
    width: '100%',
    [theme.breakpoints.up('lg')]: {
      width: '95%',
    },
    '& em': {
      backgroundColor: constStyles.highlightedGoodKeywordColor,
    },
    '& em.bad': {
      backgroundColor: constStyles.highlightedBadKeywordColor,
    },
    '& tr[style*="background-color"] + tr:not([localization])': tableStyles.MuiTableRow.root['&:hover'],
  },
  priorityColumn: {},
  languagesColumn: {
    marginBlockStart: 0,
    marginBlockEnd: 0,
    paddingInlineStart: 0,
    textAlign: 'left',
    whiteSpace: 'nowrap',
  },
});
