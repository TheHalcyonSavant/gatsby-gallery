import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  CircularProgress,
  Grid,
} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './Details.css';
import { constStyles } from './index.css';

const goodKeywords = [
  'React',
  'Material'
];
const badKeywords = [
  'German',
  'French'
];

export default connect(({
  auth: { admin: { jobs } },
}, {
  rowData: { ours: { id } },
}) => {
  const job = jobs.find(j => j && j.ours && j.ours.id === id);
  return job ? {
    ours: job.ours,
    currentLang: job.ours.currentLang,
  } : {};
}, {
  changeJobLang: (jobId, futureLang) => ({ type: 'ADMIN_CHANGE_JOB_LANG', jobId, futureLang }),
  getJobText: (id, externalId) => ({ type: 'ADMIN_GET_JOB_TEXT', id, externalId }),
  jobApplied: id => ({ type: 'ADMIN_JOB_APPLIED', id }),
})(withStyles(styles)(({
  classes, rowData, tableRef,
  ours, currentLang,
  changeJobLang, getJobText, jobApplied,
}) => {
  const { id, externalId, text, textEN, detectedLang, url, applied } = ours || {};
  const shouldGetText = ours && !textEN;
  useEffect(() => {
    if (shouldGetText) getJobText(id, externalId);
  }, [shouldGetText, getJobText, id, externalId]);
  const tableObj = tableRef.current;
  useEffect(() => {
    if (shouldGetText || !id) return;
    changeJobLang(id, 'en'); // change to EN on expand
    tableObj && tableObj.onAllSelected(false);
    rowData.tableData.checked = true;
    tableObj && tableObj.onSelectionChange(rowData);
    return () => {
      if (shouldGetText || !id) return;
      changeJobLang(id, detectedLang); // change to original language on collapse
      tableObj && tableObj.onAllSelected(false);
    };
  }, [shouldGetText, tableObj, rowData, changeJobLang, id, detectedLang]);
  const futureLang = currentLang === 'en' ? detectedLang : 'en';
  const content = (futureLang === 'en' ? text : textEN) || '';
  let iframe = null, isIframeLoaded = false;
  function resizeIframe() {
    if (isIframeLoaded) return;
    const d = iframe.contentDocument;
    const styleEl = d.createElement('style');
    const floater = d.createElement('div');
    styleEl.type = 'text/css';
    styleEl.appendChild(d.createTextNode(`
      html {
        font-size: 0.875rem;
        font-family: "Roboto", "Helvetica", "Arial", sans-serif;
        font-weight: 400;
        line-height: 1.43;
        letter-spacing: 0.01071em;
      }
      * {
        background-color: ${constStyles.selectedRowColor} !important;
      }
      em {
        background-color: ${constStyles.highlightedGoodKeywordColor} !important;
      }
      em.bad {
        background-color: ${constStyles.highlightedBadKeywordColor} !important;
      }`));
    d.querySelector('head').appendChild(styleEl);
    floater.style.cssText = `
      float: right;
      width: 55px;
      height: 30px;
    `;
    d.querySelector('body').prepend(floater);
    iframe.height = (iframe.contentWindow.document.body.scrollHeight + 30) + "px";
    isIframeLoaded = true;
  }
  return shouldGetText ? <CircularProgress
    className={classes.progress}
    size={50}
    thickness={10}
  /> : <div className={classes.details}>
    <Button className={classes.translateButton} onClick={() => changeJobLang(id, futureLang)}>{futureLang.toUpperCase()}</Button>
    <iframe
      title="details"
      className={classes.iframe}
      ref={ref => iframe = ref}
      onLoad={resizeIframe}
      scrolling="no"
      srcDoc={content
        .replace(new RegExp(goodKeywords.join('|'), 'ig'), '<em>$&</em>')
        .replace(new RegExp(badKeywords.join('|'), 'ig'), `<em class="bad">$&</em>`)
        .replace(/\\r/g, '<br/>')} />
    <Grid container justify="space-between" alignItems="center">
      <Grid item>
        <a href={url || `https://console.firebase.google.com/u/0/project/hazel-cedar-540/database/firestore/data~2Fswiss-jobs~2F${id}`}
          target="_blank" rel="noopener noreferrer">
          Visit {url ? 'page' : 'firebase'}
        </a>
      </Grid>
      <Grid item>
        <Button disabled={applied} onClick={() => jobApplied(id)}>Applied</Button>
      </Grid>
    </Grid>
  </div>;
}));
