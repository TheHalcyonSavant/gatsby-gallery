import React, { useEffect, useRef } from 'react';
import { Helmet } from 'react-helmet';
import { connect, ReactReduxContext } from 'react-redux';
import withStyles from '@material-ui/core/styles/withStyles';
import ThemeProvider from '@material-ui/styles/ThemeProvider';
import MaterialTable from 'material-table';
import moment from 'moment';
import Alert from '../../components/Alert';
import ProtectRoute from '../../components/ProtectRoute';
import Loading from '../../components/Loading';
import { changeUrl } from '../../utils';
import JobDetails from './Details';
import Toolbar from './Toolbar';
import styles, { tableStyles } from './index.css';

const TableRefresher = connect(({ auth }) => ({
  initiateQuering: auth.admin.initiateQuering,
  query: auth.admin.query,
}))(({ initiateQuering, tableRef, query }) => {
  if (tableRef.current) {
    tableRef.current.state.query = {
      ...tableRef.current.state.query,
      ...query,
      accessDB: false,
      pushHistory: true,
    }
  }
  useEffect(() => {
    if (tableRef.current.initialized) {
      // console.log('%cRefresh Table', 'color: #2141dcde', initiateQuering);  // todo: why it renders 3 times on every initiateQuering edit?
      tableRef.current.onQueryChange(tableRef.current.state.query);
      return;
    }
    tableRef.current.initialized = true;
  }, [initiateQuering, tableRef]);
  return null;
});
export default ProtectRoute('admin')(connect(({ auth }) => ({
  wait: auth.wait
}), {
  getJobStats: () => ({ type: 'ADMIN_GET_JOB_STATS' }),
  fetchJobs: () => ({ type: 'ADMIN_FETCH_JOBS' }),
  moveJobsUp: (evt, jobs) => ({ type: 'ADMIN_MOVE_UP_JOBS', jobs }),
  moveJobsDown: (evt, jobs) => ({ type: 'ADMIN_MOVE_DOWN_JOBS', jobs }),
  ignoreJobs: (evt, jobsToIgnore) => ({ type: 'ADMIN_IGNORE_JOBS', jobsToIgnore }),
  changeToolbarState: toolbarState => ({ type: 'ADMIN_JOBS_TOOLBAR_STATE_CHANGE', toolbarState }),
})(withStyles(styles)(({
  wait, classes,
  getJobStats, fetchJobs, moveJobsUp, moveJobsDown, ignoreJobs, changeToolbarState
}) => {
  const tableRef = useRef(null);
  // console.log('%cRender index', 'color: #009450de', wait);
  return (
    <div className="centered-container">
      <Helmet>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
      </Helmet>
      <Alert />
      {wait ? <Loading /> : <div className={classes.table}>
        <ReactReduxContext.Consumer>
          {({ store }) => <ThemeProvider theme={{ overrides: tableStyles }}>
          <MaterialTable
            title={<div className={classes.title}>SJS</div>}
            components={{ Toolbar }}
            // filters: jobAdvertisement.jobContent.employment: { permanent, shortEmployment }
            columns={[
              {
                title: 'Priority',
                render: ({ ours: { flags: { order } } }) => <span className={classes.priorityColumn}>{order || 0}</span>,
                type: 'numeric',
                // defaultSort: 'desc',
              },
              {
                title: 'ID',
                hidden: true,
                render: ({ ours: { id, externalId } }) => <div>{id}<br />{externalId}</div>,
              },
              {
                title: 'Title',
                render: ({ ours: { currentLang, title, titleEN } }) => (<span dangerouslySetInnerHTML={{
                  __html: currentLang === 'en' ? titleEN : title
                }}></span>),
                customSort: (a, b) => {
                  const v1 = a.ours.title;
                  const v2 = b.ours.title;
                  if (v1 < v2) return 1;
                  if (v1 > v2) return -1;
                  return 0;
                }
              },
              {
                title: 'Short Description',
                render: ({ ours: { currentLang, shortText, shortTextEN } }) => (<span dangerouslySetInnerHTML={{
                  __html: currentLang === 'en' ? shortTextEN : shortText
                }}></span>),
              },
              {
                title: 'City',
                field: 'ours.city'
              },
              {
                title: 'Company',
                field: 'ours.company'
              },
              {
                title: 'Languages',
                render: ({ ours: { languages } }) => (<ul className={classes.languagesColumn}>{languages.map((o, i) =>
                  <li key={i}>{o.languageIsoCode || o.language} {(o.level || o.spokenLevel) && `(L-${o.level || o.spokenLevel})`}</li>
                )}</ul>)
              },
              {
                title: 'www',
                cellStyle: {
                  whiteSpace: 'nowrap',
                },
                field: 'ours.webpage',
              },
              {
                title: 'Date added',
                cellStyle: {
                  minWidth: 160
                },
                render: ({ ours: { date } }) => {
                  const t = moment(date);
                  return <span>{t.format('YYYY-MM-DD')}<br />{t.format('h:mm:ss A')}</span>;
                },
                customSort: (a, b) => {
                  const v1 = a.ours.date;
                  const v2 = b.ours.date;
                  if (v1 < v2) return 1;
                  if (v1 > v2) return -1;
                  return 0;
                }
              },
            ]}
            data={query => new Promise(resolve => {
              const unsubscribe = store.subscribe(() => {
                const { auth: { admin: { jobs, totalCount, query: { page } }}, lastType } = store.getState();
                if (lastType !== 'ADMIN_JOBS_QUERIED') return;
                resolve({
                  data: jobs,
                  page,
                  totalCount,
                });
                unsubscribe();
              });
              changeUrl(query);
            })}
            tableRef={tableRef}
            options={{
              columnsButton: true,
              pageSize: 10,
              showFirstLastPageButtons: false,
              selection: true,
              rowStyle: data => data && data.tableData.checked ? tableStyles.MuiTableRow.root['&:hover'] : {}
            }}
            onSelectionChange={rows => {
              console.log('onSelectionChange', rows.length);
              changeToolbarState({ enabled: !!rows.length });
            }}
            actions={[{
              // disabled: wait, // check above description on why it's disabled
              tooltip: 'Get jobs',
              icon: 'cloud_download',
              isFreeAction: true,
              onClick: fetchJobs,
            }, {
              // disabled: wait, // check above description on why it's disabled
              tooltip: 'Get stats',
              icon: 'live_help',
              isFreeAction: true,
              onClick: getJobStats,
            }, {
              tooltip: 'Move Up',
              icon: 'keyboard_arrow_up',
              onClick: moveJobsUp
            }, {
              tooltip: 'Move Down',
              icon: 'keyboard_arrow_down',
              onClick: moveJobsDown
            }, {
              tooltip: 'Hide Jobs',
              icon: 'visibility_off',
              onClick: ignoreJobs
            }]}
            detailPanel={rowData => <JobDetails rowData={rowData} tableRef={tableRef} />}
          ></MaterialTable></ThemeProvider>}
        </ReactReduxContext.Consumer>
        <TableRefresher tableRef={tableRef} />
      </div>}
    </div>
  );
})));
