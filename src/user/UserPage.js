import React from 'react';
import { connect } from 'react-redux';
import { Container } from '@material-ui/core';
import ProtectRoute from '../components/ProtectRoute';

export default ProtectRoute()(connect(state => ({
  userMenu: state.auth.userMenu,
  pathname: window.location.pathname,
}))(({ userMenu, pathname }) => {
  const page = userMenu.find(m => ~pathname.indexOf(m.slug));
  return (
    <Container>
      <div dangerouslySetInnerHTML={{ __html: page ? page.description : '' }}></div>
    </Container>
  );
}));
