const { resolve } = require('path');
const { createFilePath } = require(`gatsby-source-filesystem`)

// exports.onPreInit = () => {
// };

exports.onCreateWebpackConfig = ({ stage, loaders, actions, getConfig }) => {
  // if (stage === 'develop') {
  //   const config = getConfig();
  //   config.module.rules.sideEffects = false;
  //   actions.replaceWebpackConfig(config)
  // }
  if (stage === 'build-javascript') {
    actions.setWebpackConfig({
      devtool: false,
    });
  }
  // if (stage === "build-html") {
  //   actions.setWebpackConfig({
  //     module: {
  //       rules: [
  //         {
  //           test: /bad-module/,
  //           use: loaders.null(),
  //         },
  //       ],
  //     },
  //   })
  // }
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;

  if (node.internal.type === `MarkdownRemark` && typeof node.slug === `undefined`) {
    createNodeField({
      node,
      name: `slug`,
      value: createFilePath({ node, getNode, basePath: `src/md` }),
    });
  }
}

exports.createPages = ({ actions: { createPage, createRedirect }, graphql }) => {
  return graphql(`{
    allMarkdownRemark {
      edges {
        node {
          fields {
            slug
          }
        }
      }
    }
  }`).then(result => {
    if (result.errors) {
      return Promise.reject(result.errors)
    }
    result.data.allMarkdownRemark.edges.forEach(({ node }) => {
      createPage({
        path: node.fields.slug,
        component: resolve('src/components/MD.template.js'),
        context: {
          slug: node.fields.slug,
        },
      })
    });
  });
}
